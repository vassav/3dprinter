#include "stm32f0xx_tim.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_rcc.h"
#include <stm32f0xx_gpio.h>
#include "MotorDriver.h"


__IO uint8_t counter;

void TIM_Config(void)
{
	InitGPIOMotor();

	NVIC_InitTypeDef NVIC_InitStructure;

	  /* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	  /* Enable the TIM3 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_Init(&NVIC_InitStructure);



	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 10;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	uint16_t PrescalerValue = (uint16_t)(SystemCoreClock  / 1500000) -1;
	TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
	



	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM3, ENABLE);
	
	
	PrescalerValue = (uint16_t)(SystemCoreClock  / 1000000) - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;

	TIM_TimeBaseStructure.TIM_Period = 100;

	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_Init(&NVIC_InitStructure);

	
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM2, ENABLE);

}

void TIM3_IRQHandler(void)
{

	TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	GPIOA->BSRR = GPIO_Pin_8;

	if (counter == 0)
		{
			TIM_Cmd(TIM3, DISABLE);
			ResetPwm(); 
			TIM_Cmd(TIM3, ENABLE);
		}
	else
		UpdatePwm(counter);

	counter++;
	GPIOA->BSRR = GPIO_Pin_8<<16;

	//GPIOA->ODR &= ~GPIO_Pin_8;

}

void TIM2_IRQHandler(void)
{
	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	Move();

}
